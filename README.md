# TextEditor
*Basic Text Editor created using Java swing* 

This project initially started as a practice in using Java Swing components to build a basic GUI.  
  Although, inspired by the contributions made to *RSyntaxTextArea* library, it has since grown to include  
  syntax highlingting for 14 languages as well as many other beneficial features for programmers.  
  
This file requires that you include **RSyntaxTextArea.jar** library in order to compile.  
  Which you may download here: https://github.com/bobbylight/RSyntaxTextArea/  
  

**FEATURES:**  
- Syntax highlighting for 14 languages
- Bracket matching
- Code folding
- Line numbering
- Current cursor position display
- "*Monokai*" inspired color scheme
- ***In Developement:***  
  * Font Chooser
  * User selected/customizable Themes
  * Multi-tab support
  
**ISSUES:**  
- Theme importing via XML is not working as intended. Issue with opening ResourceStream.  
- "Spaces to Tabs" feature is not funtioning properly. Reason Unknown.
- CaretListener throwing *NullPointerException* when caret position changes and is also no longer updating display.
