/*
 * TODO: Add bottom toolbar w/ current line number and line index
 * TODO: Add changeable themes/xml theme importing
 * TODO: Add selectable fonts menu
 * TODO: Implement "Find" search abilities
 * TODO: Change selected highlight color
 * TODO: Fix "Spaces to Tabs" ability
 *
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.*;
import javax.swing.border.*;
import org.fife.ui.rtextarea.*;
import org.fife.ui.rsyntaxtextarea.*;


class TextEditor extends JFrame {

	private RSyntaxTextArea textArea = new RSyntaxTextArea(25, 80);
	private JFileChooser dialog = new JFileChooser(System.getProperty("user.dir"));
	private JToolBar nav;
	private String currentFile = "Untitled";
	private Font menuFont = new Font("Droid Serif", Font.PLAIN, 14);
	private Font textFont = new Font("Ubuntu Mono", Font.PLAIN, 18);
	private Color textColor = new Color(210, 210, 210);
	private Color menuText = new Color(200, 200, 200);
	private Color menuColor = new Color(40, 40, 39);
	private Color backgroundColor = new Color(46, 46, 46);
	private Color commentColor = new Color(128, 128, 128);
	private int caretLine;
	private int caretColumn;
	private boolean docChange = false;


	public TextEditor() {
		JFrame frame = new JFrame();
		frame.setSize(600, 1000);
		setTitle("Text-Editor: " + currentFile);
		createMenuBar();
		createToolBar();
		//createNavBar();
		createTextArea();
		createScrollBar();
		createNavBar();
		textArea.requestFocus();
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	private void createTextArea() {
		textArea.setSize(new Dimension(600, 1000));
		Border textBorder = BorderFactory.createLineBorder(menuColor, 5);
		textArea.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		textArea.setBorder(textBorder);
		textArea.setMargin(new Insets(80, 80, 80, 80));
		textArea.setBackground(backgroundColor);
		textArea.setForeground(textColor);
		textArea.setCaretColor(textColor);
		textArea.setCaretPosition(0);
		textArea.getCaret().setVisible(true);
		textArea.setFont(textFont);
		Color selectedColor = new Color(56, 58, 56);
		textArea.setCurrentLineHighlightColor(selectedColor);
		textArea.setMarkAllHighlightColor(selectedColor);
		textArea.setMarkOccurrencesColor(selectedColor);

		textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
		// Uses Java by default
		textArea.setBracketMatchingEnabled(true);
		textArea.setCodeFoldingEnabled(true);
		textArea.setMatchedBracketBGColor(selectedColor);
		textArea.setMatchedBracketBorderColor(textColor);
		textArea.setPaintMatchedBracketPair(true);
		textArea.setTabLineColor(menuColor); //FIXME: not working?
		textArea.setPaintTabLines(true);
		changeStyleManually();
		textArea.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent e) {
//				if(nav.getComponentAtIndex(0) != null) {
					nav.remove(1);
					nav.add(getCaretPosition());
//				} else
//					nav.add(getCaretPosition());
			}
		});
		textArea.requestFocus();
	}


	private void createScrollBar() {
		RTextScrollPane scroll = new RTextScrollPane(textArea);
		scroll.setBackground(menuColor);
		scroll.setForeground(commentColor);
		scroll.setFont(new Font("Droid Serif", Font.PLAIN, 14));
		add(scroll, BorderLayout.CENTER);
	}

	private void createMenuBar() {
		JMenuBar JMB = new JMenuBar() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2 = (Graphics2D) g;
				g2.setPaint(menuColor);
				g2.fillRect(0, 0, getWidth(), getHeight());
			}
		};
		setJMenuBar(JMB);
		JMB.setForeground(textColor);
		JMB.setOpaque(false);
		JMB.setFont(menuFont);
		JMenu file = new JMenu("File");
		JMenu edit = new JMenu("Edit");
		JMenu view = new JMenu("View");
		JMenu syntax = new JMenu("Syntax");
		JMenu help = new JMenu("Help");
		JMB.add(file);
		JMB.add(edit);
		JMB.add(view);
		JMB.add(syntax);
		JMB.add(help);

		/*  File  */
		ArrayList<Action> fileActions = new ArrayList<>();
		fileActions.add(New);
		fileActions.add(Open);
		fileActions.add(Save);
		fileActions.add(SaveAs);
		fileActions.add(Preference);
		fileActions.add(Quit);
		String fileList = "New_Open_Save_Save As_Preference_Quit";
		String[] fileItems = fileList.split("_");
		for(int i = 0; i < fileActions.size();i++) {
			fileItems[i] = fileItems[i].replaceAll("\\s","");
			Image img = new ImageIcon("images/"+fileItems[i]+"2.png").getImage().getScaledInstance(15,15,java.awt.Image.SCALE_SMOOTH);
			file.add(fileActions.get(i)).setIcon(new ImageIcon(img));
			if(i == 3 || i == 4)
				file.addSeparator();
		}

		/*  Edit  */
		ArrayList<Action> editActions = new ArrayList<>();
		editActions.add(Cut);
		editActions.add(Copy);
		editActions.add(Paste);
		editActions.add(Select);
		editActions.add(SelectAll);
		editActions.add(PageUp);
		editActions.add(PageDown);
		String editList = "Cut_Copy_Paste_Select_Select All_Page Up_Page Down";
		String[] editItems = editList.split("_");
		for(int i = 0; i < editActions.size();i++) {
			editItems[i] = editItems[i].replaceAll("\\s","");
			Image img = new ImageIcon("images/"+editItems[i]+"2.png").getImage().getScaledInstance(15,15,java.awt.Image.SCALE_SMOOTH);
			edit.add(editActions.get(i)).setIcon(new ImageIcon(img));
			if(i == 2 || i == 4)
				edit.addSeparator();
		}
		int k = 0;
		while (k < edit.getItemCount()) {
			for (String menuItem : editItems) {
				JMenuItem item;
				Component component = edit.getMenuComponent(k);
				if (component instanceof JSeparator)
					item = edit.getItem(++k);
				else
					item = edit.getItem(k);
				item.setText(menuItem);
				k++;
			}
		}

		/*  View  */
		JMenu selectFont = new JMenu("Font");
		view.add(selectFont);
		selectFont.setText("Font");
		selectFont.setIcon(new ImageIcon("images/Font2.png"));
		view.add(selectFont);
		selectFont.add(optionFont);
		selectFont.getItem(0).setBackground(menuColor);
		selectFont.getItem(0).setForeground(menuText);
		JMenu FontSize = new JMenu("Font Size");
		FontSize.setIcon(new ImageIcon("images/Font2.png"));
		view.add(FontSize);
		for (int i = 5; i < 33; i++) {
			JMenuItem item = new JMenuItem(++i + "");
			FontSize.add(item);
			item.setBackground(menuColor);
			item.setForeground(menuText);
			item.setFont(menuFont);
			item.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					textArea.setFont(new Font("Ubuntu Mono", Font.PLAIN, Integer.parseInt(e.getActionCommand())));
				}
			});
		}
		view.addSeparator();
		view.add(Tabs);  //FIXME
		view.addSeparator();
		view.add(Themes);

		/*  Synatax */
		syntax.setFont(menuFont);
		syntax.setBackground(menuColor);
		syntax.setForeground(textColor);
		String syntaxList = "C C++ C# CSS HTML Java JavaScript JSON Lua Perl Ruby SQL UNIX WinBatch";
		String[] syntaxItems = syntaxList.split(" ");
		for (String lang : syntaxItems) {
			JMenuItem item = new JMenuItem(lang);
			item.setMnemonic(lang.charAt(0));
			syntax.add(item);
			item.addActionListener(e -> setSyntax(e.getActionCommand()));
		}

		/*  Help   */
		help.add(None).setFont(menuFont);
		help.getItem(0).setText("Coming Soon... ");

		// Set color of menu items
		for (int j = 0; j < JMB.getMenuCount(); j++) {
			JMenu menu = JMB.getMenu(j);
			for (int i = 0; i < JMB.getMenu(j).getItemCount(); i++) {
				JMenuItem item = JMB.getMenu(j).getItem(i);
				Component component = JMB.getMenu(j).getMenuComponent(i);
				if (!(component instanceof JSeparator)) {
					item.setBackground(menuColor);
					item.setForeground(menuText);
					item.setFont(menuFont);
					item.setOpaque(true);
				}
			}
			menu.setBackground(menuColor);
			menu.setForeground(menuText);
			menu.setFont(menuFont);
			menu.setOpaque(true);
		}
	}


	private void createToolBar() {
		JToolBar tool = new JToolBar() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2D = (Graphics2D) g;
				g2D.setPaint(menuColor);
				g2D.fillRect(0, 0, getWidth(), getHeight());
			}
		};
		ArrayList<JButton> buttonList = new ArrayList<>();
		tool.setFloatable(false);
		tool.setRollover(true);
		tool.setBackground(menuColor);
		tool.setForeground(menuColor);
		tool.setBorderPainted(false);
		add(tool, BorderLayout.NORTH);
		tool.addSeparator(new Dimension(15, getHeight()));
		JButton newb = tool.add(New);
		newb.setName("New");
		buttonList.add(newb);
		JButton open = tool.add(Open);
		open.setName("Open");
		buttonList.add(open);
		JButton save = tool.add(Save);
		save.setName("Save");
		buttonList.add(save);
		tool.addSeparator(new Dimension(15, getHeight()));
		JButton cut = tool.add(Cut);
		cut.setName("Cut");
		buttonList.add(cut);
		JButton copy = tool.add(Copy);
		copy.setName("Copy");
		buttonList.add(copy);
		JButton paste = tool.add(Paste);
		paste.setName("Paste");
		buttonList.add(paste);

		for(JButton item : buttonList) {  // sets all necessary parameters for toolbar items
			item.setBackground(menuColor);
			item.setBorderPainted(false);
			item.setText(null);
			ImageIcon imgIcon = new ImageIcon("images/"+item.getName()+"2.png");
			Image img = imgIcon.getImage();
			Image scaleImg = img.getScaledInstance(15, 16, java.awt.Image.SCALE_SMOOTH);
			item.setIcon(new ImageIcon(scaleImg));
			item.setToolTipText(item.getName());
		}

//		Save.setEnabled(false);
//		SaveAs.setEnabled(false);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();

		KeyListener keyList = new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				docChange = true;
				Save.setEnabled(true);
				SaveAs.setEnabled(true);
			}
		};
		textArea.addKeyListener(keyList);
		setVisible(true);
	}

	/*  Action Map  */
	ActionMap map = textArea.getActionMap();
	Action New = new AbstractAction("New", new ImageIcon("images/New2.png")) {
		public void actionPerformed(ActionEvent e) {
			new TextEditor().setSize(new Dimension(getWidth(),getHeight()));
		}
	};
	Action Open = new AbstractAction("Open", new ImageIcon("images/Open2.png")) {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			if (dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
				readInFile(dialog.getSelectedFile().getAbsolutePath());
			SaveAs.setEnabled(true);
		}
	};
	Action Save = new AbstractAction("Save", new ImageIcon("images/Save2.png")) {
		public void actionPerformed(ActionEvent e) {
			if (!currentFile.equals("Untitled"))
				saveFile(currentFile);
			else
				saveFileAs();
		}
	};
	Action SaveAs = new AbstractAction("Save as...", new ImageIcon("images/SaveAs2.png")) {
		public void actionPerformed(ActionEvent e) {
			saveFileAs();
		}
	};
	Action Preference = new AbstractAction("Preferences") {
		@Override
		public void actionPerformed(ActionEvent e) {
			JPopupMenu pref = new JPopupMenu("Preferences");
			pref.setSize(300, 200);
			pref.setVisible(true);
		}
	};
	Action Quit = new AbstractAction("Quit") {
		public void actionPerformed(ActionEvent e) {
			saveOld();
			System.exit(0);
		}
	};
	Action Cut = map.get(DefaultEditorKit.cutAction);
	Action Copy = map.get(DefaultEditorKit.copyAction);
	Action Paste = map.get(DefaultEditorKit.pasteAction);
	Action Select = map.get(DefaultEditorKit.selectionNextWordAction);
	Action SelectAll = map.get(DefaultEditorKit.selectAllAction);
	Action PageUp = map.get(DefaultEditorKit.pageUpAction);
	Action PageDown = map.get(DefaultEditorKit.pageDownAction);
	Action optionFont = new AbstractAction("This Font ") {    // TODO: Font Change Option
		public void actionPerformed(ActionEvent e) {
			String userFont = e.getActionCommand();
			changeFont(userFont);
		}
	};
	Action Tabs = new AbstractAction("Convert Spaces to Tabs ") { //FIXME
		public void actionPerformed(ActionEvent e) {
			textArea.convertSpacesToTabs();
		}
	};  // FIXME
	Action Themes = new AbstractAction("Themes ") {    // TODO: Theme Change Option
		public void actionPerformed(ActionEvent e) {
			//String chosen = e.getActionCommand();
			//changeStyle("default");//chosen);
		}
	};  // FIXME
	Action selectFont = null;   // TODO:
	Action None = null;   // TODO:

	//Action Undo = map.get(DefaultEditorKit.undoAction);
	//Action Bold = map.get(StyledEditorKit.boldAction);
	//Action Italic = map.get(StyledEditorKit.italicAction);
	//Action Underline = map.get(StyledEditorKit.underlineAction);


	private void createNavBar() {
		nav = new JToolBar() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2D = (Graphics2D) g;
				g2D.setPaint(menuColor);
				g2D.fillRect(0, 0, getWidth(), getHeight());
			}
		};
		nav.setFloatable(false);
		nav.setRollover(true);
		nav.setBorderPainted(false);
		add(nav, BorderLayout.SOUTH);
		nav.setBackground(menuColor);
		nav.setForeground(menuColor);
		getCaretPosition();
		nav.setAlignmentX(RIGHT_ALIGNMENT);
		nav.addSeparator(new Dimension(5, nav.getHeight()));
		nav.add(getCaretPosition());
		nav.setVisible(true);
	}

//	public JTextPane setCaretPosition(int caretLine, int caretColumn) {
//		JTextPane caretText = new JTextPane();
//		caretText.setBackground(menuColor);
//		caretText.setForeground(commentColor);
//		caretText.setFont(new Font("Droid Serif", Font.PLAIN, 14));
//		caretText.setText("Line:"+ caretLine + "  Col:" + caretColumn);
//		return caretText;
//	}

	public JTextPane getCaretPosition() {
		JTextPane caretText = new JTextPane();
		caretText.setAlignmentX(RIGHT_ALIGNMENT);
		caretLine = textArea.getCaretLineNumber() + 1;
		caretColumn = textArea.getCaretOffsetFromLineStart() + 1;
		caretText.setFont(new Font("Droid Serif", Font.PLAIN, 14));
		caretText.setText("Line:"+ caretLine + "  Col:" + caretColumn);
		caretText.setBackground(menuColor);
		caretText.setForeground(commentColor);
		return caretText;
	}


	private void saveFileAs() {
		if (dialog.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			saveFile(dialog.getSelectedFile().getAbsolutePath());
	}

	private void saveOld() {
		if (docChange)
			if (JOptionPane.showConfirmDialog(this, "Would you like to save " + currentFile + " ?", "Save", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION)
				saveFile(currentFile);
	}

	private void readInFile(String filename) {
		try {
			FileReader fr = new FileReader(filename);
			textArea.read(fr, null);
			fr.close();
			currentFile = filename;
			setTitle(currentFile);
			docChange = false;
		} catch (IOException e) {
			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog(this, "The Editor can't find the selected file: " + filename);
		}
	}

	private void saveFile(String filename) {
		try {
			FileWriter w = new FileWriter(filename);
			textArea.write(w);
			w.close();
			currentFile = filename;
			setTitle(currentFile);
			docChange = false;
			Save.setEnabled(false);
		} catch (IOException e) {
			System.out.println("**ERROR**: Unable to save file.");
		}
	}


	private void changeFont(String userFont) {
		setFont(textArea, new Font(userFont, Font.PLAIN, 18));
	}

	private static void setFont(RSyntaxTextArea textArea, Font font) {
		if (font != null) {
			SyntaxScheme scheme = textArea.getSyntaxScheme();
			scheme = (SyntaxScheme) scheme.clone();
			for (int i = 0; i < scheme.getStyleCount(); i++) {
				if (scheme.getStyle(i) != null) {
					scheme.getStyle(i).font = font;
				}
			}
			textArea.setSyntaxScheme(scheme);
			textArea.setFont(font);
		}
	}

	private void setSyntax(String syntax) {
		System.out.println("Syntax: " + syntax);
		switch (syntax) {
			case "C":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_C);
				break;
			case "C++":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);
				break;
			case "C#":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CSHARP);
				break;
			case "CSS":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CSS);
				break;
			case "HTML":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_HTML);
				break;
			case "Java":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
				break;
			case "JavaScript":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
				break;
			case "JSON":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JSON);
				break;
			case "Lua":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_LUA);
				break;
			case "Perl":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PERL);
				break;
			case "Python":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PERL);
				break;
			case "SQL":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
				break;
			case "UNIX":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_UNIX_SHELL);
				break;
			case "WinBatch":
				textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_WINDOWS_BATCH);
				break;
		}
	}

//	private void setTheme() { //FIXME: Issue with changing themes via XML import
//		//String filePath = "themes/eclipse_theme.xml";
//		InputStream in = getClass().getResourceAsStream("/dark.xml");
//		try {
//			Theme theme = Theme.load(in);
//			theme.apply(textArea);
////			Theme theme = new Theme(textArea);
////			theme.load(this.getClass().getResourceAsStream("/TextEditor/themes/eclipse_theme.xml"));
////			theme.apply(textArea);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

	/* Manually sets syntax highlighting themes
	 * Current color configuration based on "Monokai" theme
	 */
	private void changeStyleManually() {
		SyntaxScheme scheme = textArea.getSyntaxScheme();
		Color pink = new Color(249, 38, 114);
		Color orange = new Color(253, 151, 31);
		Color yellow = new Color(220, 229, 119);
		Color green = new Color(166, 226, 46);
		Color blue = new Color(82, 227, 246);
		Color purple = new Color(174, 129, 255);
		scheme.getStyle(Token.IDENTIFIER).foreground = new Color(249, 249, 249);
		scheme.getStyle(Token.RESERVED_WORD).foreground = pink;
		scheme.getStyle(Token.RESERVED_WORD_2).foreground = green;
		scheme.getStyle(Token.DATA_TYPE).foreground = green;
		scheme.getStyle(Token.OPERATOR).foreground = pink;
		scheme.getStyle(Token.FUNCTION).foreground = blue;
		scheme.getStyle(Token.VARIABLE).foreground = orange;
		scheme.getStyle(Token.SEPARATOR).foreground = new Color(240, 240, 240); //brackets
		scheme.getStyle(Token.LITERAL_STRING_DOUBLE_QUOTE).foreground = yellow; //strings
		scheme.getStyle(Token.LITERAL_NUMBER_DECIMAL_INT).foreground = purple; //#'s
		scheme.getStyle(Token.LITERAL_NUMBER_HEXADECIMAL).foreground = orange;
		scheme.getStyle(Token.LITERAL_BOOLEAN).foreground = orange; //true/false
		scheme.getStyle(Token.COMMENT_EOL).font = new Font("Druid Serif", Font.ITALIC, 16);
		scheme.getStyle(Token.COMMENT_EOL).foreground = commentColor;
		scheme.getStyle(Token.ANNOTATION).foreground = orange;
		scheme.getStyle(Token.MARKUP_TAG_NAME).foreground = orange; //nothing
		scheme.getStyle(Token.MARKUP_TAG_ATTRIBUTE).foreground = orange; //nothing
		scheme.getStyle(Token.MARKUP_ENTITY_REFERENCE).foreground = orange; //nothing
		scheme.getStyle(Token.REGEX).foreground = orange;
		scheme.getStyle(Token.DATA_TYPE).font = textFont;
	}


	public static void main(String[] args) {
		TextEditor editor = new TextEditor();
		editor.setSize(new Dimension(800, 900));
	}

}
